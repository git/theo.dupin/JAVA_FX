package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Captor;

import java.util.List;

public class ImageWindow extends CaptorMonitorWindow {

    @FXML
    private Button buttonExit;
    @FXML
    private ImageView imageView;
    private Captor capteur;
    private List<Captor> listeCapteurs;


    public ImageWindow(List<Captor> liste){
        this.listeCapteurs = liste;
    }

    public void update() {
        if (capteur.getTemp() <= 0) {
            this.imageView.setImage(new javafx.scene.image.Image("/img/froid.png"));
        } else if (capteur.getTemp() >= 40) {
            this.imageView.setImage(new javafx.scene.image.Image("/img/chaud.png"));
        } else {
            this.imageView.setImage(new javafx.scene.image.Image("/img/moyen.png"));
        }
    }

    public void initialize(){
        this.imageView.setImage(new Image("/img/chaud.png"));
        this.update();
    }

    public void exit(){
        Stage stage = (Stage) buttonExit.getScene().getWindow();
        stage.close();
    }
}
