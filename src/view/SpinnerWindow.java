package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Captor;

import java.util.ArrayList;
import java.util.List;

public class SpinnerWindow extends CaptorMonitorWindow {
    @FXML
    private Button buttonExit;
    @FXML
    private Spinner<Captor> spinner;
    @FXML
    private TextField textField;
    private List<Captor> captorList = new ArrayList<>();

    public void exit(){
        Stage stage = (Stage) buttonExit.getScene().getWindow();
        stage.close();
    }
}
