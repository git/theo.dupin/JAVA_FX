package view;

import javafx.stage.Stage;

public class ControlWindow extends Stage {

    private String title;
    private String pathToFXMLRessource;


    public ControlWindow(){}
    public ControlWindow(String title,String pathToFXMLRessource){
        this.title = title;
        this.pathToFXMLRessource = pathToFXMLRessource;
    }

    public String getTitleFXMLWindow() {
        return this.title;
    }

    public void setTitleFXMLWindow(String title) {
        this.title = title;
    }

    public String getPathToFXMLRessource(){
        return this.pathToFXMLRessource;
    }

    public void setPathToFXMLRessource(String pathToFXMLRessource) {
        this.pathToFXMLRessource = pathToFXMLRessource;
    }

}
