package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import model.CaptorBasique;

import java.io.IOException;

public class CaptorMonitorWindow extends VBox {
    @FXML
    private Label idCapteur;
    @FXML
    private TextField nomCapteur;

    CaptorBasique captorBasique;


    public void CaptorMonitorWindow(CaptorBasique c){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CaptorMonitorWindow.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.captorBasique = c;

        idCapteur.setText("ID: " + captorBasique.getId());
        nomCapteur.setText(captorBasique.getName());
    }
}
