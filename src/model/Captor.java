package model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.UUID;

public abstract class Captor extends Observable {
    // Id Property
    private StringProperty id = new SimpleStringProperty();
    public String getId(){
        return id.get();
    }
    public void setId(String newId){
        id.set(newId);
    }
    public StringProperty idProperty(){
        return id;
    }

    // Name property
    private StringProperty name = new SimpleStringProperty();
    public String getName(){
        return name.get();
    }
    public void setName(String newName){
        name.set(newName);
    }
    public StringProperty nameProperty(){
        return name;
    }

    // Temperature property
    private DoubleProperty temp = new SimpleDoubleProperty();
    public double getTemp(){
        return temp.get();
    }
    public void setTemp(double newTemp){
        temp.set(newTemp);
    }
    public DoubleProperty tempProperty(){
        return temp;
    }
    // Generation
    private String generationStrategy;

    Captor(String name){
        this.setName(name);
        this.setId(UUID.randomUUID().toString());
        this.setTemp(0);
    }

    public void setStrategie(String s){
        this.generationStrategy = s;
    }

    public String getGenerationStrategy() {
        return this.generationStrategy;
    }

    public void add(Captor captor,double coef){}

    public abstract void run();

    public String toString(){
        return getName();
    }
}
