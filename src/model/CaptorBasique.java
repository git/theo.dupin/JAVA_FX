package model;

import javafx.application.Platform;

public class CaptorBasique extends Captor {
    private GenerationStrategy generationStrategy;
    private int poids = 0;
    private int delay = 1000;
    public CaptorBasique(String nom, GenerationStrategy generation) {
        super(nom);
        this.generationStrategy = generation;
        if(!(generationStrategy instanceof GenerationRandom)){
            Thread t = new Thread((Runnable) this);
            t.setDaemon(true);
            t.start();
        }

    }

    @Override
    public void run(){
        while (true){
            try{
                Platform.runLater( () -> this.setTemp(generationStrategy.generator()));
                Thread.sleep(this.delay);
            }
            catch (InterruptedException e){
                    break;
            }
        }
    }

    @Override
    public String toString() {
        return "[" + super.getId() + "] " + super.getName() + ": " + generationStrategy.toString();
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public GenerationStrategy getGenStrat() {
        return generationStrategy;
    }

    public void setGenStrat(GenerationStrategy generationStrategy) {
        this.generationStrategy = generationStrategy;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}
