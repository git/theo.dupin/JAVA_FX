package model;

public class GenerationRandom implements GenerationStrategy{
    @Override
    public double generator() {
        double tmp = Math.random()/Math.nextDown(1);
        return 0*(1 - tmp) + 50 * tmp;
    }
}
