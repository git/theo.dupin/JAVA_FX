package model;

import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;

public class GenerationCPU implements GenerationStrategy {

    public GenerationCPU() {
        generator();
    }

    @Override
    public double generator() {
        OperatingSystemMXBean sysBean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        double temperature = sysBean.getSystemCpuLoad() * 100;
        System.out.println("La température du CPU est de " + temperature + "°C");
        return temperature;
    }
}
