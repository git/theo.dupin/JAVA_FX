package model;

public interface GenerationStrategy {

    public abstract double generator();
}
