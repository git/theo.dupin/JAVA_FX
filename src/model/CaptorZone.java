package model;

import javafx.application.Platform;

import java.util.ArrayList;
import java.util.List;

public class CaptorZone extends Captor{
    private List<Captor> listeCapteurs = new ArrayList<Captor>();

    private int poids = 0;

    private String generationStrategy = "Capteur Virtuel";

    public CaptorZone(String nom) {
        super(nom);
        Thread t = new Thread((Runnable) this);
        t.setDaemon(true);
        t.start();
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public String getGenerateurStrategique() {
        return generationStrategy;
    }

    @Override
    public String toString() {
        return "[" + super.getId() + "] " + super.getName() + ": Virtuel";
    }

    public void addToList(Captor c){
        this.listeCapteurs.add(c);
    }

    public void removeToList(Captor c){
        listeCapteurs.remove(c);
    }

    public List<Captor> getListeCapteurs() {
        return listeCapteurs;
    }

    @Override
    public void run(){
        while (true){
            try{
                Platform.runLater(() -> {
                    if(listeCapteurs.isEmpty()) return;
                    double temperatureMoyenne = 0;
                    int err = 0;
                    for(Captor c : listeCapteurs){
                        if(c == null){
                            err++;
                            break;
                        }
                        temperatureMoyenne += c.getTemp();
                    }
                    this.setTemp(temperatureMoyenne/(listeCapteurs.size()-err));
                }  );
                Thread.sleep(1000);
            }
            catch (InterruptedException e){
                break;
            }
        }
    }
}
