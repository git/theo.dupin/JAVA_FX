package model;

import java.util.List;

public class Observable {
    protected List<Observer> observers;

    public void attach(Observer o) {
        observers.add(o);
    }

    public void detach(Observer o) {
        observers.remove(o);
    }

    public void notifyObserver() {
        for(Observer o : observers) {
            o.update();
        }
    }
}
