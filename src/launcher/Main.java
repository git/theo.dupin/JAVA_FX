package launcher;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import model.Captor;

public class Main extends Application {

    @FXML
    private TreeView<Captor> treeView;
    @FXML
    private TreeItem<Captor> root;

    @FXML
    private TextField texte;


    @Override
    public void start(Stage stage) throws Exception {
        // Fenêtre Principale
        Parent monitorWindow = FXMLLoader.load(getClass().getResource("/fxml/CaptorMonitorWindow.fxml"));
        Stage monitorWindowStage = new Stage();
        monitorWindowStage.setScene(new Scene(monitorWindow));
        monitorWindowStage.setTitle("CaptorMonitorWindow");
        monitorWindowStage.show();

        // Fenêtre Image
        Parent image = FXMLLoader.load(getClass().getResource("/fxml/ImageWindow.fxml"));
        Stage imageStage = new Stage();
        imageStage.setScene(new Scene(image));
        imageStage.setTitle("ImageWindow");
        imageStage.show();

        // Fenêtre Spinner
        /*Parent spinner = FXMLLoader.load(getClass().getResource("/fxml/SpinnerWindow.fxml"));
        Stage spinnerStage = new Stage();
        spinnerStage.setScene(new Scene(spinner));
        spinnerStage.setTitle("SpinnerWindow");
        spinnerStage.show();*/
    }
}
